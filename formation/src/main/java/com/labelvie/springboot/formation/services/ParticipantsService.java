package com.labelvie.springboot.formation.services;

import com.labelvie.springboot.formation.Models.Participants;

import java.util.List;

public interface ParticipantsService {
    Participants saveParticipant(Participants participants);
    List<Participants> getAllParticipants();
    Object getParticipantById(long id);
    Participants updateParticipant(Participants participants, long id);
    void deleteParticipant(long id);
}
