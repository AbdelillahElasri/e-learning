package com.labelvie.springboot.formation.Models;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Table(name = "`admin`")
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "Admin_name")
    private String Admin_name;
    @Column(name = "Admin_email")
    private String Admin_email;
    @Column(name = "Admin_password")
    private String Admin_password;
    @ManyToMany
    @JoinTable(
            name = "admin_specialties",
            joinColumns = @JoinColumn(name = "admin_id"),
            inverseJoinColumns = @JoinColumn(name = "specialty_id")
    )
    private List<Specialties> specialties;
}
