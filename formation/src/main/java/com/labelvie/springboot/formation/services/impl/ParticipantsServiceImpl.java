package com.labelvie.springboot.formation.services.impl;

import com.labelvie.springboot.formation.Models.Participants;
import com.labelvie.springboot.formation.Models.Test;
import com.labelvie.springboot.formation.Repository.ParticipantsRepository;
import com.labelvie.springboot.formation.exception.ResourceNotFoundException;
import com.labelvie.springboot.formation.services.ParticipantsService;

import java.util.List;
import java.util.Optional;

public class ParticipantsServiceImpl implements ParticipantsService {

    private ParticipantsRepository participantsRepository;

    public ParticipantsServiceImpl(ParticipantsRepository participantsRepository) {
        super();
        this.participantsRepository = participantsRepository;
    }

    @Override
    public Participants saveParticipant(Participants participants) {
        return participantsRepository.save(participants);
    }

    @Override
    public List<Participants> getAllParticipants() {
        return participantsRepository.findAll();
    }

    @Override
    public Object getParticipantById(long id) {
        Optional<Participants> participants = participantsRepository.findById(id);
        if (participants.isPresent()){
            participants.get();
        }
        return participantsRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Participants","Id",id));
    }

    @Override
    public Participants updateParticipant(Participants participants, long id) {
        // we need to check whether test with given id is exist in DB or not
        Participants existingParticipant = participantsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Participant", "Id", id));
        existingParticipant.setParticipant_username(participants.getParticipant_username());
        existingParticipant.setParticipant_name(participants.getParticipant_name());
        existingParticipant.setParticipant_email(participants.getParticipant_email());
        existingParticipant.setParticipant_address(participants.getParticipant_address());
        existingParticipant.setParticipant_collage_id(participants.getParticipant_collage_id());
        existingParticipant.setParticipant_password(participants.getParticipant_password());
        existingParticipant.setParticipant_level(participants.getParticipant_level());
        existingParticipant.setParticipant_mobile(participants.getParticipant_mobile());
        // save existing test to DB
        participantsRepository.save(existingParticipant);
        return existingParticipant;
    }

    @Override
    public void deleteParticipant(long id) {
        // check whether a test exist in a DB or not
        participantsRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Participant","Id",id));
        participantsRepository.deleteById(id);
    }
}
