package com.labelvie.springboot.formation.Models;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Table(name = "`Participants`")
public class Participants {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "Participant_username")
    private String Participant_username;
    @Column(name = "Participant_password")
    private String Participant_password;
    @Column(name = "Participant_email")
    private String Participant_email;
    @Column(name = "Participant_name")
    private String Participant_name;
    @Column(name = "Participant_collage_id")
    private Long Participant_collage_id;
    @Column(name = "Participant_address")
    private String Participant_address;
    @Column(name = "Participant_level")
    private String Participant_level;
    @Column(name = "Participant_mobile")
    private String Participant_mobile;
    @ManyToMany
    @JoinTable(
            name = "participant_course",
            joinColumns = @JoinColumn(name = "participant_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id")
    )
    private List<Course> courses;





}
