package com.labelvie.springboot.formation.Repository;

import com.labelvie.springboot.formation.Models.Participants;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipantsRepository extends JpaRepository<Participants, Long> {
}
